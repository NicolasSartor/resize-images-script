const Jimp = require('jimp');
const fs = require('fs');
const path = require('path');

const imagesDir = path.join(__dirname, 'images');
const resizedDir = path.join(__dirname, 'resized');

const outputWidth = 400;

if (!fs.existsSync(resizedDir)) {
  fs.mkdirSync(resizedDir);
}

fs.readdir(imagesDir, (err, files) => {
  if (err) {
    console.error("Could not list the directory.", err);
    process.exit(1);
  }

  files.forEach(file => {
    const inputPath = path.join(imagesDir, file);
    const outputPath = path.join(resizedDir, file.split('.').slice(0, -1).join('.') + '.webp');

    // Read the image with Jimp
    Jimp.read(inputPath)
      .then(image => {
        return image
          .resize(outputWidth, Jimp.AUTO)
          .quality(70)
          .write(outputPath);
      })
      .then(() => {
        console.log('Processed file:', file);
      })
      .catch(err => {
        console.error('Error processing file:', file, err);
      });
  });
});